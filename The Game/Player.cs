﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Game
{
    class Player
    {
        private bool turn;
        private int bank;
        private List<BoardAndPieces.Piece> pieces = new List<BoardAndPieces.Piece>();
        public Player(bool turn)
        {
            setTurn(turn);
            bank = 10;
        }
        public int getBank()
        {
            return bank;
        }
        private void setBank(int bank)
        {
            this.bank = bank;
        }
        public void deductFromBank(int amountToDeduct)
        {
            setBank(this.bank + amountToDeduct);
        }
        public void addToBank(int amountToAdd)
        {
            setBank(this.bank + amountToAdd);
        }
        public bool isTurn()
        {
            return turn;
        }
        private void setTurn(bool turn)
        {
            this.turn = turn;
        }
        public void beginTurn()
        {
            setTurn(true);
        }
        public void endTurn(bool isTurn)
        {
            setTurn(true);
        }
        public List<BoardAndPieces.Piece> getPieces()
        {
            return pieces;
        }
        public void setPieces(List<BoardAndPieces.Piece> pieces)
        {
            this.pieces = pieces;
        }
        public void addPiece(BoardAndPieces.Piece pieceToAdd)
        {
            pieces.Add(pieceToAdd);
        }
    }
}
