﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Game.BoardAndPieces
{
    public class Board
    {
        public List<Spot> gameBoard = new List<Spot>();

        public Board()
        {
            int x = 0;
            while (x <= 63)
            {
                CreateEmptySpot(x);
                x++;
            }
        }

        public bool IsOccupied(int x)
        {
            return gameBoard[x].IsOccupied();
        }
        public Spot GetSpot(int position)
        {
            return gameBoard[position];
        }
        public void CreateEmptySpot(int position)
        {
            Spot emptySpot = new Spot(position);
            gameBoard.Add(emptySpot);
        }
        public void PrintBoard()
        {
            Console.Write("_________________________________\n");
            int x = 0;
            while (x <= gameBoard.Count)
            {
                if (x % 8 == 0)
                {
                    if (x != 0)
                    {
                        Console.Write(" | \n");
                    }
                    Console.Write("| ");
                }
                else
                {
                    Console.Write(" | ");
                }
                if (gameBoard[x].IsOccupied())
                {
                    switch (gameBoard[x].contains)
                    {
                        case Piece.ObjectReference.PAWN:
                            {
                                Console.Write("P");
                                break;
                            }
                        case Piece.ObjectReference.KNIGHT:
                            {
                                Console.Write("K");
                                break;
                            }
                        case Piece.ObjectReference.BISHOP:
                            {
                                Console.Write("B");
                                break;
                            }
                        case Piece.ObjectReference.ROOK:
                            {
                                Console.Write("R");
                                break;
                            }
                        case Piece.ObjectReference.QUEEN:
                            {
                                Console.Write("Q");
                                break;
                            }
                        case Piece.ObjectReference.KING:
                            {
                                Console.Write("*");
                                break;
                            }
                        case Piece.ObjectReference.TOKEN:
                            {
                                Console.Write(".");
                                break;
                            }
                        case Piece.ObjectReference.EMPTY:
                            {
                                Console.Write(" ");
                                break;
                            }
                    }
                }
                else
                {
                    Console.Write(" ");
                }

                x++;
            }
            Console.Write(" | ");
            Console.Write("\n_________________________________\n");
        }
    }
}
