﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Game.BoardAndPieces
{
    public class Piece
    {
        public enum ObjectReference
        {
            PAWN,
            KNIGHT,
            BISHOP,
            ROOK,
            QUEEN,
            KING,
            TOKEN,
            EMPTY
        }
        protected ObjectReference thisObject;
        protected int x;
        public Piece(int x, Board gameBoard)
        {
            Place(x, gameBoard);
        }
        public void Place(int x, Board gameBoard)
        {
            try
            {
                if (gameBoard.IsOccupied(x))
                    Console.Write("Bad placement exception");
                gameBoard.gameBoard[x].contains = this.thisObject;
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }
        public static void Move(int x, int y)
        {
            Console.Write("Not yet implemented");
        }   
    }
}
