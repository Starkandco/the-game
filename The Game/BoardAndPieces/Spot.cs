﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Game.BoardAndPieces
{
    public class Spot
    {
        readonly int x;
        readonly int y;
        public Piece.ObjectReference contains;

        public Spot(int position)
        {
            this.x = position % 8;
            this.y = y / 8;
            contains = Piece.ObjectReference.EMPTY;
        }

        public bool IsOccupied()
        {
            if (contains == Piece.ObjectReference.EMPTY || contains == Piece.ObjectReference.TOKEN)
                return false;
            return true;
        }

        public void ReleaseSpot()
        {
            this.contains = Piece.ObjectReference.EMPTY;
        }
    }
}
