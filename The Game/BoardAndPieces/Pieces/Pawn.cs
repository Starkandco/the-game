﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Game.BoardAndPieces.Pieces
{
    public class Pawn : Piece
    {
        public Pawn(int x, Board gameBoard) 
        {
            this.x = x;
            this.thisObject = ObjectReference.PAWN;
            Place(x, gameBoard);
        }   
    }
}
