﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Game
{
    class Program
    {
        static void Main(string[] args)
        {
            BoardAndPieces.Board gameBoard = new BoardAndPieces.Board();
            BoardAndPieces.Piece thisPiece = new BoardAndPieces.Piece(0, gameBoard);
            gameBoard.PrintBoard();
            Console.ReadKey();
        }

    }
}
