﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using The_Game.BoardAndPieces;

namespace The_Game_Test.BoardAndPiecesTest
{
    [TestClass]
    public class BoardTest
    {
        [TestMethod]
        public void CreateEmptySpotTest()
        {
            Random rand = new Random();
            int x = rand.Next(0, 64);
            Board newBoard = new Board();
            Assert.AreEqual(newBoard.gameBoard.Count, 64);
            newBoard.CreateEmptySpot(x);
            Assert.AreEqual(newBoard.gameBoard.Count, 65);
        }
        [TestMethod]
        public void GetSpotTest()
        {
            Random rand = new Random();
            int x = rand.Next(0, 64);
            Board newBoard = new Board();
            Assert.AreEqual(Piece.ObjectReference.EMPTY, newBoard.GetSpot(x).contains);
        }
        [TestMethod]
        public void IsOccuppiedTest()
        {
            Random rand = new Random();
            int x = rand.Next(0, 64);
            Board newBoard = new Board();
            Assert.IsFalse(newBoard.IsOccupied(x));
            Piece newPiece = new Piece(x, newBoard);
            Assert.IsTrue(newBoard.IsOccupied(x));
        }
    }
}
