﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using The_Game.BoardAndPieces;

namespace The_Game_Test.BoardAndPiecesTest
{
    [TestClass]
    public class SpotTest
    {
        [TestMethod]
        public void IsOccupiedTest()
        {
            Random rand = new Random();
            int x = rand.Next(0, 64);
            Spot newSpot = new Spot(x);
            Assert.IsFalse(newSpot.IsOccupied());
            Board newBoard = new Board();
            Piece newPiece = new Piece(x, newBoard);
            Assert.IsTrue(newBoard.gameBoard[x].IsOccupied());
        }
        [TestMethod]
        public void ReleaseSpot()
        {
            Random rand = new Random();
            int x = rand.Next(0, 64);
            Board newBoard = new Board();
            Piece newPiece = new Piece(x, newBoard);
            newBoard.gameBoard[x].ReleaseSpot();
            Assert.AreEqual(Piece.ObjectReference.EMPTY, newBoard.gameBoard[x].contains);
        }
    }
}
