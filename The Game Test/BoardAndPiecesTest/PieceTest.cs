﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using The_Game.BoardAndPieces;

namespace The_Game_Test.BoardAndPiecesTest
{
    [TestClass]
    public class PieceTest
    {
        [TestMethod]
        public void PlaceTest()
        {
            Random rand = new Random();
            int x = rand.Next(0, 64);
            Board newBoard = new Board();
            Piece newPiece = new Piece(x, newBoard);
            Assert.AreEqual(newPiece.thisObject, newBoard.gameBoard[x].contains);
            Assert.IsTrue(newBoard.gameBoard[x].IsOccupied());
        }
    }
}
